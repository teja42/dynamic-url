const app = new require('express').Router();
const AES = require('./modules/strongAES');
const crypto = require('crypto');

app.get("/", (req,res) => {
   res.render('home.ejs');
});

app.get("/get-url", (req,res) => {
   let url = {
      ts: Date.now(),
      key: crypto.randomBytes(12).toString('base64') // session key
   };
   url = JSON.stringify(url);

   try{
      let aes = new AES(global.$config.aesKey);
      url = aes.encrypt(url);
   } catch(e){
      return res.status(500).send();
   }

   res.redirect(302, `/submit/${url}`);

});

app.use('/submit/:formId', (req,res,next) => {
try{
   let props = req.params['formId'];
   let aes = new AES(global.$config.aesKey);
   props = aes.decrypt(props);
   props = JSON.parse(props);
   req.$props = props;
   next();
} catch(e) {
   console.log("Decrypt error");
   res.sendStatus(400);
}
});

app.get('/submit/:formId', (req,res) => {
try{
   // check if url has been visited before
   // If yes redirect to a new url
   if(req.cookies['form_id'] == req.params.formId) {
      return res.redirect(301, '/get-url');
   }

   res.cookie('form_id', req.params.formId , {
      path: '/submit'
   });

   let aes = new AES(req.$props.key);

   let form = [
      {
         name: "Input 1",
         data: aes.encrypt('inp1')
      },
      {
         name: "Input 2",
         data: aes.encrypt('inp2')
      },
      {
         name: "Input 3",
         data: aes.encrypt('inp3')
      },
      {
         name: "Input 4",
         data: aes.encrypt('inp4')
      }
   ]

   res.render('form.ejs', {form});

} catch(e){
   res.sendStatus(400);
}
});

const usedFormIds = {};

app.post('/submit/:formId', (req,res) => {
let err = "";
try{
   // Time limit check
   if(Date.now() < (req.$props.ts + 20*1000 ) ) { // It hasn't been 20 secs yet
      err = 'Error : Waiting time not reached';
      console.log(err);
      throw new Error();
   }

   // Link is valid only for 5 minutes
   if(req.$props.ts + 5*60*1000 <= Date.now() ) { // expired
      err = "Error: Expired link";
      console.log(err);
      throw new Error();
   }

   // check if this url has been used to submit before
   // This requires the use of a Database to store used formId's 
   // and check if a formId has been used before
   // But since this is just a demo I'm using an object to store used formId's. 
   if(usedFormIds[req.params.formId]) {
      err = 'Error : Form Id has already been used before';
      console.log(err);
      throw new Error();
   } else {
      usedFormIds[req.params.formId] = true;
   }

   // Process form data
   let body = req.body;
   let form = {};
   let aes = new AES(req.$props.key);
   for (el in body) {
      let _el = aes.decrypt(el);
      form[_el] = body[el];
   }

   console.log(form);
   res.sendStatus(200);
} catch(e){
   if(err) 
      res.status(400).send(err);
   else 
   res.sendStatus(400);
}
});

module.exports = app;