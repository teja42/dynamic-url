const crypto = require('crypto');

module.exports = function(key) {
   let aesKey = key;
   let sha2 = crypto.createHash('sha256');
   sha2.update(aesKey);
   const ENCRYPTION_KEY = sha2.digest();
   const IV_LENGTH = 16;
   
   function encrypt(text) {
      let iv = crypto.randomBytes(IV_LENGTH);
      let cipher = crypto.createCipheriv('aes-256-gcm', new Buffer(ENCRYPTION_KEY), iv);
      let encrypted = cipher.update(text,'utf8','hex');
      encrypted += cipher.final('hex');
      let tag = cipher.getAuthTag().toString('hex');
      iv = iv.toString('hex');
      return `${tag}${iv}${encrypted}`;
   }
   
   function decrypt(text) {
      let tag = new Buffer(text.substr(0,32),'hex'); // extract from 0 - 31 chars
      let iv = new Buffer(text.substr(32,32),'hex'); // extract from 32 - 64 chars
      let encryptedText = new Buffer(text.substr(64,text.length-1), 'hex');
      let decipher = crypto.createDecipheriv('aes-256-gcm', new Buffer(ENCRYPTION_KEY), iv);
      decipher.setAuthTag(tag);
      let decrypted = decipher.update(encryptedText,'hex','utf8');
      decrypted += decipher.final('utf8');
      return decrypted;
   }

   return {
      encrypt,
      decrypt
   }

}