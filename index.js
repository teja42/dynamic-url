const express = require("express");
const https = require("https");
const fs = require('fs');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const cookieParser = require('cookie-parser');

global.$config = {
   aesKey: "@#$%^GFDSNGTRDas"
};

const router = require('./router');

const PRIVATE_KEY = fs.readFileSync('./ssl/key.pem');
const CERTIFICATE = fs.readFileSync('./ssl/certificate.pem');

let app = express();

app.use('/static',express.static('./static'));

ejs.delimiter = '?';
app.set('view engine','ejs');

app.use(bodyParser.urlencoded({
   extended: false
}));
app.use(cookieParser());

app.use(router);

let server = https.createServer({
   key: PRIVATE_KEY,
   cert: CERTIFICATE
},app);

server.listen(4040, ()=>{
   console.log('Listening on port 4040. Visit https://localhost:4040 ');
});