# Setup

First of all make sure you have `nodejs` and `npm` installed. Then : <br/>

1. `git clone git@gitlab.com:teja42/dynamic-url.git`
2. `cd dynamic-url`
3. `npm i`

# Run

Navigate to project root and run command `npm start`

<br/>

Everything useful is printed to console